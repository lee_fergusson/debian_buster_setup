#-------------------------------------------------------------------------------
# Debian 10 (Buster) System Install Script
# by Lee Fergusson
#
# Note: Should only be run on a minimal console only fresh install
#       of Debian Buster and should be executed as root
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Upgrade to Bullseye
#-------------------------------------------------------------------------------
mv /etc/apt/sources.list /etc/apt/sources.list.bak
echo "deb http://deb.debian.org/debian/ bullseye main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian/ bullseye main contrib non-free" >> /etc/apt/sources.list

apt -y update
apt -y upgrade
apt -y dist-upgrade
apt -y autoremove

#-------------------------------------------------------------------------------
# Install Command Line Tools
#-------------------------------------------------------------------------------
apt -y install sudo
apt -y install build-essential
apt -y install meson
apt -y install doxygen
apt -y install curl
apt -y install zsh
apt -y install vim
apt -y install emacs
apt -y install ranger
apt -y install htop
apt -y install neofetch

#-------------------------------------------------------------------------------
# Install login manager
#-------------------------------------------------------------------------------
apt -y install lightdm
apt -y install lightdm-gtk-greeter lightdm-gtk-greeter-settings

#-------------------------------------------------------------------------------
# Install the desktop environment
#-------------------------------------------------------------------------------
# Install epiphany to disable Firefox
apt -y install epiphany-browser
apt -y install gnome-core
apt -y install gnome-shell-extension-dash-to-panel
apt -y install gnome-shell-extension-caffeine
apt -y install gnome-shell-extension-arc-menu

# Set the default to graphical mode
systemctl set-default graphical.target

#-------------------------------------------------------------------------------
# Install the brave browser
#-------------------------------------------------------------------------------
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
source /etc/os-release
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ bullseye main" | tee /etc/apt/sources.list.d/brave-browser-release-${bullseye}.list
apt -y update
apt -y install brave-browser

#-------------------------------------------------------------------------------
# Install My Applications
#-------------------------------------------------------------------------------
apt -y install evolution
apt -y install lollypop
apt -y install vlc
apt -y install krita
apt -y install blender
apt -y install gimp
apt -y install deja-dup
apt -y install timeshift
apt -y install gufw

#-------------------------------------------------------------------------------
# Install Fonts
#-------------------------------------------------------------------------------
apt -y install fonts-noto-core
apt -y install fonts-hack
apt -y install ttf-mscorefonts-installer

#-------------------------------------------------------------------------------
# Misc
#-------------------------------------------------------------------------------
apt -y install mono-runtime
apt -y install gtk-sharp3
apt -y install gtk2-engines-murrine
apt -y install libsdl-ttf2.0-0

#-------------------------------------------------------------------------------
# Setup User defaults
#-------------------------------------------------------------------------------
cp ./config/user /etc/dconf/profile/
mkdir /etc/dconf/db/local.d
cp ./config/00-extensions /etc/dconf/db/local.d/
dconf update

